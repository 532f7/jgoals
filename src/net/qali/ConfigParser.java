package net.qali;

import net.qali.state.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class ConfigParser {
    private static final Logger logger = Logger.getLogger(ConfigParser.class.getName());

    private static final String SECTION_WORLD = "WORLD";
    private static final String SECTION_PREREQS = "PREREQS";
    private static final String SECTION_ACTIONS = "ACTIONS";
    private static final String SECTION_ENTITIES = "ENTITIES";
    private static final String ACTION_TARGET_PREREQ_PREFIX = State.TARGET + ".";

    private static Optional<Integer> attemptNumericParse(String str) {
        try {
            return Optional.of(Integer.parseInt(str));
        } catch (NumberFormatException e) {
            return Optional.empty();
        }
    }

    private static Object parseIntegerOrString(String str) {
        Object value;
        var maybeNumber = attemptNumericParse(str);
        if (maybeNumber.isPresent()) {
            value = maybeNumber.get();
        } else {
            // fall back to string
            value = str;
        }
        return value;
    }

    private static Prerequisite parsePrereq(String str) throws IllegalArgumentException {
        String[] tokens = tokenize(str);
        if (tokens.length < 3) {
            String msg = "expected prerequisite to have at least 3 fields, but got " + tokens.length + "\n" +
                    "expected format: name property =/!=/</>/isset/isnull/contains [value...]";
            throw new IllegalArgumentException(msg);
        }

        String name = tokens[0];
        String property = tokens[1];
        if (property.equals("target")) {
            throw new IllegalArgumentException("target property must be uppercase: 'TARGET'");
        }


        Prerequisite.Comparison comparison;
        switch (tokens[2]) {
            case "=":
                comparison = Prerequisite.Comparison.Equal;
                break;
            case "!=":
                comparison = Prerequisite.Comparison.NotEqual;
                break;
            case "<":
                comparison = Prerequisite.Comparison.LessThan;
                break;
            case ">":
                comparison = Prerequisite.Comparison.GreaterThan;
                break;
            case "isset":
                comparison = Prerequisite.Comparison.IsSet;
                break;
            case "isnull":
                comparison = Prerequisite.Comparison.IsNull;
                break;
            case "in":
                comparison = Prerequisite.Comparison.In;
                break;
            default:
                throw new IllegalArgumentException("unknown comparison operator '" + tokens[2] + "'");
        }

        boolean unaryOp = comparison == Prerequisite.Comparison.IsSet || comparison == Prerequisite.Comparison.IsNull;
        boolean isContains = comparison == Prerequisite.Comparison.In;
        if (unaryOp) {
            return new Prerequisite(name, property, comparison, null);
        } else if (isContains) {
            PropertyValue value = PropertyValue.parseSet(Arrays.copyOfRange(tokens, 3, tokens.length));
            return new Prerequisite(name, property, comparison, value);
        }

        return new Prerequisite(name, property, comparison, PropertyValue.parse(tokens[3]));
    }

    private static Effect parseEffect(String str) throws IllegalArgumentException {
        String[] tokens = tokenize(str);
        if (tokens.length != 3 && tokens.length != 2) {
            String msg = "expected effect to only have 2 or 3 fields, but got " + tokens.length + "\n" +
                    "expected format: property =/+/-/delete [property_value]";
            throw new IllegalArgumentException(msg);
        }

        String property = tokens[0];
        if (property.equals("target")) {
            throw new IllegalArgumentException("target property must be uppercase: 'TARGET'");
        }

        Effect.Operator operator;
        switch (tokens[1]) {
            case "=":
                operator = Effect.Operator.Set;
                break;
            case "+":
                operator = Effect.Operator.Add;
                break;
            case "-":
                operator = Effect.Operator.Subtract;
                break;
            case "delete":
                operator = Effect.Operator.Delete;
                break;
            default:
                throw new IllegalArgumentException("unknown effect operator '" + tokens[1] + "'");
        }

        PropertyValue value = operator == Effect.Operator.Delete ? null : PropertyValue.parse(tokens[2]);
        return new Effect(property, operator, value);
    }

    private static void validateWorld(Map<Integer, Set<Integer>> world) throws IllegalArgumentException {
        for (var cell : world.entrySet()) {
            for (var neighbour : cell.getValue()) {
                if (!world.containsKey(neighbour)) {
                    throw new IllegalArgumentException("cell " + cell.getKey() + " has non-existent neighbour " + neighbour);
                }
            }
        }
    }

    private static void validateAndAddActionPrerequisite(Map<String, Prerequisite> allPrerequisites,
                                                         List<String> actionPrerequisites,
                                                         String prerequisiteName) {
        if (!allPrerequisites.containsKey(prerequisiteName)) {
            throw new IllegalArgumentException("unknown prerequisite '" + prerequisiteName + "'");
        }

        if (actionPrerequisites.contains(prerequisiteName)) {
            throw new IllegalArgumentException("duplicate prerequisite '" + prerequisiteName + "'");
        }

        actionPrerequisites.add(prerequisiteName);
    }

    static State loadStateFromFile(String filePath) throws IOException, ParsingException {
        Path path = Paths.get(filePath);
        BufferedReader reader = Files.newBufferedReader(path);

        int lineNum = 1;
        String line = reader.readLine();
        // strip leading empty lines/comments
        while (line.strip().isEmpty() || line.stripLeading().charAt(0) == '#') {
            lineNum++;
            line = reader.readLine();
        }

        ParsingState state = ParsingState.World_Header;

        int cellCID = 0;
        Set<Integer> neighbours = new HashSet<>();

        Map<Integer, Set<Integer>> world = new HashMap<>();
        Map<Integer, String> cellNames = new HashMap<>();
        List<Map<String, Object>> entities = new ArrayList<>();
        Map<String, Prerequisite> prereqs = new HashMap<>();

        String actionName = "";
        List<String> actionPrerequisites = new ArrayList<>();
        List<String> actionTargetPrerequisites = new ArrayList<>();
        List<Effect> actionEffects = new ArrayList<>();
        Map<String, Action> actions = new HashMap<>();

        String entityName = "ERRORTHISSHOULDNEVERHAPPEN";
        List<Action> entityActions = new ArrayList<>();
        Set<String> entityActionNames;
        Map<String, Object> entityProperties = new HashMap<>();

        String[] tokens = new String[0];

        int numSpaces;
        boolean reRun = false;
        while (state != ParsingState.End) {
            switch (state) {
                case World_Header:
                    if (!SECTION_WORLD.equals(line)) {
                        throw new ParsingException(lineNum, line, "first line of file should be " + SECTION_WORLD);
                    }
                    state = ParsingState.World_CID;
                    break;
                case World_CID:
                    try {
                        tokens = tokenize(line);
                        cellCID = Integer.parseInt(tokens[0]);
                        if (world.containsKey(cellCID)) {
                            throw new ParsingException(lineNum, line, "duplicate cell ID " + cellCID);
                        }
                        neighbours = new HashSet<>();
                        if (tokens.length > 1) {
                            // take everything after the first space
                            cellNames.put(cellCID, line.substring(line.indexOf(" ") + 1).strip());
                        }
                        state = ParsingState.World_Neighbour;
                    } catch (NumberFormatException e) {
                        throw new ParsingException(lineNum, line, "expected a numeric cell ID here but couldn't parse it (" + e + ")");
                    }
                    break;
                case World_Neighbour:
                    try {
                        neighbours.add(Integer.parseInt(line.strip()));
                    } catch (NumberFormatException e) {
                        throw new ParsingException(lineNum, line, "expected a numeric neighbour cell ID here but couldn't parse it (" + e + ")");
                    }
                    state = ParsingState.World_Neighbour_Or_World_CID_Or_Prereqs_Header;
                    break;
                case World_Neighbour_Or_World_CID_Or_Prereqs_Header:
                    var isNumeric = attemptNumericParse(line.strip()).isPresent();
                    tokens = tokenize(line);

                    if (line.stripLeading().equals(line) && attemptNumericParse(tokens[0]).isPresent()) {
                        state = ParsingState.World_CID;
                    } else if (isNumeric) {
                        state = ParsingState.World_Neighbour;
                    } else {
                        state = ParsingState.Prereqs_Header;
                    }

                    // end of current cell. add all its neighbours
                    if (state != ParsingState.World_Neighbour) {
                        world.put(cellCID, neighbours);
                    }

                    // we're done parsing all world entries. validate the world
                    if (state == ParsingState.Prereqs_Header) {
                        validateWorld(world);
                    }
                    reRun = true;
                    break;
                case Prereqs_Header:
                    if (!SECTION_PREREQS.equals(line)) {
                        throw new ParsingException(lineNum, line, "expected PREREQS section to start here");
                    }
                    state = ParsingState.Prereqs_Or_Action_Header;
                    break;
                case Prereqs_Or_Action_Header:
                    // heuristic, if there are no space then it's a header
                    numSpaces = line.split(" ").length - 1;
                    if (numSpaces == 0) {
                        state = ParsingState.Actions_Header;
                    } else {
                        state = ParsingState.Prereq;
                    }
                    reRun = true;
                    break;
                case Prereq:
                    try {
                        Prerequisite prereq = parsePrereq(line);
                        if (prereqs.containsKey(prereq.name)) {
                            throw new ParsingException(lineNum, line, "duplicate prerequisite '" + prereq.name + "'");
                        }
                        prereqs.put(prereq.name, prereq);
                    } catch (IllegalArgumentException e) {
                        throw new ParsingException(lineNum, line, "unable to parse prerequisite: " + e.getMessage());
                    }
                    state = ParsingState.Prereqs_Or_Action_Header;
                    break;
                case Actions_Header:
                    if (!SECTION_ACTIONS.equals(line)) {
                        throw new ParsingException(lineNum, line, "expected ACTIONS section to start here");
                    }
                    state = ParsingState.Action_Or_Entities_Header;
                    break;
                case Action_Or_Entities_Header:
                    // heuristic, if there are no space then it's a header
                    tokens = tokenize(line);
                    // disambiguate between a zero-prereq action and the actual entities header
                    if (tokens.length == 1 && tokens[0].equals(SECTION_ENTITIES)) {
                        state = ParsingState.Entities_Header;
                    } else {
                        state = ParsingState.Action_Start;
                    }
                    reRun = true;
                    break;
                case Action_Start:
                    tokens = tokenize(line);
                    actionName = tokens[0];
                    actionEffects = new ArrayList<>();
                    actionPrerequisites = new ArrayList<>();
                    actionTargetPrerequisites = new ArrayList<>();
                    for (int i = 1; i < tokens.length; i++) {
                        boolean isTargetPrereq = tokens[i].startsWith(ACTION_TARGET_PREREQ_PREFIX);
                        if (isTargetPrereq) {
                            validateAndAddActionPrerequisite(prereqs, actionTargetPrerequisites, tokens[i].substring(tokens[i].indexOf(".") + 1));
                        } else {
                            validateAndAddActionPrerequisite(prereqs, actionPrerequisites, tokens[i]);
                        }
                    }
                    state = ParsingState.Action_Effect;
                    break;
                case Action_Effect:
                    try {
                        Effect e = parseEffect(line);
                        actionEffects.add(e);
                        state = ParsingState.Action_Effect_Or_Action_Start_Or_ActionOrEntitiesHeader;
                        break;
                    } catch (IllegalArgumentException e) {
                        throw new ParsingException(lineNum, line, "can't parse effect for action '" + actionName + "': " + e.getMessage());
                    }
                case Action_Effect_Or_Action_Start_Or_ActionOrEntitiesHeader:
                    // action effect if leading space
                    tokens = tokenize(line);
                    if (line.startsWith(" ") || line.startsWith("\t")) {
                        state = ParsingState.Action_Effect;
                    } else if (tokens.length == 1 && tokens[0].equals(SECTION_ENTITIES)) { // disambiguate between a zero-prereq action and the actual entities header
                        state = ParsingState.Entities_Header;
                    } else {
                        state = ParsingState.Action_Start;
                    }

                    // pop the current action if we're done parsing it
                    if (state != ParsingState.Action_Effect) {
                        actions.put(actionName, new Action(actionName, actionEffects,
                                actionPrerequisites.stream()
                                        .map(prereqs::get)
                                        .collect(Collectors.toList()),
                                actionTargetPrerequisites.stream()
                                        .map(prereqs::get)
                                        .collect(Collectors.toList())));
                    }
                    reRun = true;
                    break;
                case Entities_Header:
                    if (!SECTION_ENTITIES.equals(line)) {
                        throw new ParsingException(lineNum, line, "expected ENTITIES section to start here");
                    }
                    state = ParsingState.Entity_Start;
                    break;
                case Entity_Start:
                    entityActions = new ArrayList<>();
                    entityProperties = new HashMap<>();
                    entityActionNames = new HashSet<>();

                    tokens = tokenize(line);
                    entityName = tokens[0];
                    for (int i = 1; i < tokens.length; i++) {
                        actionName = tokens[i];
                        if (!actions.containsKey(actionName)) {
                            throw new ParsingException(lineNum, line, "entity '" + entityName + "' has nonexistent action '" + actionName + "'");
                        }
                        if (entityActionNames.contains(actionName)) {
                            throw new ParsingException(lineNum, line, "entity '" + entityName + "' has duplicate action '" + actionName + "'");
                        }

                        entityActionNames.add(actionName);
                        entityActions.add(actions.get(actionName));
                    }
                    state = ParsingState.Entity_Prop_Or_Entity_Start;
                    break;
                case Entity_Prop_Or_Entity_Start:
                    if (line == null || line.strip().isEmpty()) {
                        // EOF-ish
                        state = ParsingState.End;
                    } else if (!line.stripLeading().equals(line)) {
                        // line starts with whitespace so it might be a property
                        state = ParsingState.Entity_Prop;
                    } else {
                        // attempt to parse as a new entity
                        state = ParsingState.Entity_Start;
                    }

                    // if we're at the end of the current entity, add it to our list
                    if (state != ParsingState.Entity_Prop) {
                        entityProperties.put(State.ACTIONS, entityActions);
                        entityProperties.put(State.NAME, entityName);
                        entities.add(entityProperties);
                    }

                    reRun = true;
                    break;
                case Entity_Prop:
                    tokens = tokenize(line);
                    if (State.RESERVED_PROPERTIES.contains(tokens[0])) {
                        throw new ParsingException(lineNum, line, "entity '" + entityName + "' cannot use reserved property '" + tokens[0] + "'");
                    }

                    if (tokens[0].equals("goals")) {
                        throw new ParsingException(lineNum, line, "entity '" + entityName + "' has invalid GOALS property: must be uppercase: 'GOALS'");
                    }

                    if (tokens[1].equals("pre_action")) {
                        throw new ParsingException(lineNum, line, "entity '" + entityName + "' has invalid PRE_ACTION property: must be uppercase: 'PRE_ACTION'");
                    }

                    if (tokens[0].equals(State.GOALS)) {
                        state = ParsingState.Entity_Prop_Goals;
                    } else if (tokens[0].equals(State.PRE_ACTION)) {
                        state = ParsingState.Entity_Prop_PreAction;
                    } else {
                        state = ParsingState.Entity_Prop_Simple;
                    }
                    reRun = true;

                    break;
                case Entity_Prop_Goals:
                    if (tokens.length == 1) {
                        throw new ParsingException(lineNum, line, "entity '" + entityName + "' has invalid GOALS property: must contain at least 1 prerequisite");
                    }
                    List<Prerequisite> goals = new ArrayList<>(tokens.length);
                    for (int i = 1; i < tokens.length; i++) {
                        if (!prereqs.containsKey(tokens[i])) {
                            throw new ParsingException(lineNum, line,
                                    "entity '" + entityName + "' has invalid GOALS property: unknown prerequisite '" + tokens[i] + "'");
                        }

                        goals.add(prereqs.get(tokens[i]));
                    }
                    entityProperties.put(State.GOALS, goals);
                    state = ParsingState.Entity_Prop_Or_Entity_Start;
                    break;
                case Entity_Prop_PreAction:
                    if (tokens.length == 1) {
                        throw new ParsingException(lineNum, line, "entity '" + entityName + "' has invalid PRE_ACTION property: must contain at least 1 action");
                    }
                    List<Action> preActions = new ArrayList<>(tokens.length);
                    for (int i = 1; i < tokens.length; i++) {
                        if (!actions.containsKey(tokens[i])) {
                            throw new ParsingException(lineNum, line,
                                    "entity '" + entityName + "' has invalid PRE_ACTION property: unknown action '" + tokens[i] + "'");
                        }

                        preActions.add(actions.get(tokens[i]));
                    }
                    entityProperties.put(State.PRE_ACTION, preActions);
                    state = ParsingState.Entity_Prop_Or_Entity_Start;
                    break;
                case Entity_Prop_Simple:
                    if (tokens.length != 2) {
                        throw new ParsingException(lineNum, line, "entity '" + entityName + "' has invalid property definition, expected 2 fields but got " + tokens.length);
                    }
                    if (tokens[0].equals("prop")) {
                        throw new ParsingException(lineNum, line, "entity '" + entityName + "' has invalid prop property: must be uppercase: 'PROP'");
                    }
                    entityProperties.put(tokens[0], parseIntegerOrString(tokens[1]));
                    state = ParsingState.Entity_Prop_Or_Entity_Start;
                    break;
            }

            // re-parse/we're transitioning
            if (reRun) {
                reRun = false;
                continue;
            }

            do {
                line = reader.readLine();
                lineNum++;
            } while (line != null && (line.strip().isEmpty() || line.stripLeading().charAt(0) == '#'));
        }

        State parsedState = new State(world, cellNames);
        for (var entity : entities) {
            parsedState.addEntity(entity);
        }
        return parsedState;
    }

    private static String[] tokenize(String str) {
        return str.trim().split("\\s+");
    }


    enum ParsingState {
        World_Header,
        World_CID,
        World_Neighbour,
        World_Neighbour_Or_World_CID_Or_Prereqs_Header,
        Prereqs_Header,
        Prereqs_Or_Action_Header,
        Prereq,
        Actions_Header,
        Action_Start,
        Action_Effect,
        Action_Effect_Or_Action_Start_Or_ActionOrEntitiesHeader,
        Action_Or_Entities_Header,
        Entities_Header,
        Entity_Start,
        Entity_Prop_Or_Entity_Start,
        Entity_Prop,
        Entity_Prop_Simple,
        Entity_Prop_Goals,
        Entity_Prop_PreAction,
        End,
    }
}
