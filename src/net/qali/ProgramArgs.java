package net.qali;

public class ProgramArgs {
    private static final int DEFAULT_TICKS = 30;
    private static final String usage = "usage: jgoals filename [ticks]\n" +
            "arguments:\n" +
            "\tfilename\tpath to state file for simulation\n" +
            "\tticks\tnumber of ticks to simulate (default: 30)";
    public final String filename;
    public final int numTicks;

    public ProgramArgs(String[] args) throws Exception {
        if (args.length < 1 || args.length > 2) {
            System.err.println("invalid args");
            System.err.println(usage);
            throw new Exception("invalid args");
        }

        filename = args[0];
        if (args.length == 2) {
            try {
                numTicks = Integer.parseInt(args[1]);
            } catch (NumberFormatException e) {
                System.err.println("can't parse ticks '" + args[1] + "'");
                System.err.println(usage);
                System.exit(1);
                throw new Exception("can't parse ticks '" + args[1] + "'");
            }
        } else {
            numTicks = DEFAULT_TICKS;
        }
    }
}
