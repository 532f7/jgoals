package net.qali.state;


import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class Action {
    private static final Logger logger = Logger.getLogger(Action.class.getName());

    public final String name;
    public final List<Effect> effects;
    public final List<Prerequisite> prerequisites;
    public final List<Prerequisite> targetPrerequisites;

    public Action(String name,
                  List<Effect> effects,
                  List<Prerequisite> prerequisites,
                  List<Prerequisite> targetPrerequisites) {
        this.name = name;
        this.effects = effects;
        this.prerequisites = Collections.unmodifiableList(prerequisites);
        this.targetPrerequisites = Collections.unmodifiableList(targetPrerequisites);
    }

    public boolean possible(State state, int eid) {
        for (var prereq : prerequisites) {
            if (!prereq.eval(state, eid)) {
                return false;
            }
        }

        if (!targetPrerequisites.isEmpty()) {
            Optional<Integer> targetEid = state.getTarget(eid);
            if (targetEid.isEmpty()) {
                throw new IllegalStateException(String.format("cannot check action %s: entity '%s' (%d) has no target",
                        this, state.getName(eid), eid));
            }

            for (var targetPrereq : targetPrerequisites) {
                if (!targetPrereq.eval(state, targetEid.get())) {
                    return false;
                }
            }
        }

        return true;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("action=(%s requires:", name));
        sb.append(prerequisites.stream().map(Prerequisite::toString).collect(Collectors.joining(" , ")));

        sb.append(" effects:");
        sb.append(effects.stream().map(Effect::toString).collect(Collectors.joining(", ")));

        sb.append(")");
        return sb.toString();
    }

    public void apply(State state, int eid) {
        assert possible(state, eid) : "attempted to apply a not-possible action "; // remove eventually

        for (var effect : effects) {
            effect.apply(state, eid);
        }
    }

    /**
     * Returns whether or not this action uses any properties from its entity's TARGET.
     */
    public boolean isTargeted() {
        for (Effect e : effects) {
            if (e.isTargeted()) {
                return true;
            }
        }
        return false;
    }
}
