package net.qali.state;

import java.util.Optional;
import java.util.Set;

public class Prerequisite {
    public final String name;
    public final String property;
    public final Comparison comparison;

    private final boolean isNumeric;
    private final PropertyValue value;


    public Prerequisite(String name, String property, Comparison comparison, PropertyValue value) {
        this.name = name;
        this.property = property;
        this.comparison = comparison;
        this.value = value;
        this.isNumeric = comparison == Comparison.LessThan || comparison == Comparison.GreaterThan;

        if (comparison == Comparison.In && value.type != PropertyValue.ValueType.Set) {
            throw new UnsupportedOperationException(
                    String.format("invalid prerequisite '%s': cannot apply set operation to non-set value '%s'",
                            name, value));
        }

        if (isNumeric && !value.isNumeric()) {
            throw new UnsupportedOperationException(
                    String.format("invalid prerequisite '%s': cannot apply numeric comparison '%s' to non-number '%s'",
                            name, comparison, value));
        }
    }

    public boolean eval(State state, int eid) {
        var lhs = state.getProperty(eid, property);

        if (this.comparison == Comparison.IsSet) {
            return lhs != null;
        } else if (this.comparison == Comparison.IsNull) {
            return lhs == null;
        } else if (this.comparison == Comparison.In) {
            Optional<Object> maybeSet = value.eval(state, eid);
            assert maybeSet.isPresent() : "set operator returned null";

            if (lhs == null) {
                return false;
            }

            Set<PropertyValue> props = (Set<PropertyValue>) maybeSet.get();
            for (var prop : props) {
                var rhs = prop.eval(state, eid);
                if (rhs.isPresent() && rhs.get().equals(lhs)) {
                    return true;
                }
            }

            return false;
        }

        var maybeRhs = value.eval(state, eid);

        // any prereqs which rely on null/missing LHS will fail
        if (lhs == null || maybeRhs.isEmpty()) {
            return false;
        }


        var rhs = maybeRhs.get();

        if (isNumeric && (lhs.getClass() != Integer.class || rhs.getClass() != Integer.class)) {
            String msg = String.format("unable to eval %s: numeric type mismatch for comparison %s %s %s",
                    this.toString(), lhs, comparison, rhs);
            throw new UnsupportedOperationException(msg);
        }
        switch (this.comparison) {
            case Equal:
                return lhs.equals(rhs);
            case NotEqual:
                return !lhs.equals(rhs);
            case LessThan:
                return ((Integer) lhs).compareTo((Integer) rhs) < 0;
            case GreaterThan:
                return ((Integer) lhs).compareTo((Integer) rhs) > 0;
            default:
                throw new IllegalStateException(
                        String.format("couldn't determine numeric comparison operator for prerequisite '%s': %s",
                                name, comparison));
        }
    }

    public String toString() {
        return String.format("prerequisite={%s [%s %s %s]}", name, property, comparison, value);
    }

    public enum Comparison {
        Equal,
        NotEqual,
        LessThan,
        GreaterThan,
        IsSet,
        IsNull,
        In,
    }
}
