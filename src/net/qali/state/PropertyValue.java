package net.qali.state;

// represents the RHS of an effect or prerequisite. can either be a fixed value or

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Represents the RHS of an effect or prerequisite. Can either be a fixed numeric or string value, or (if it starts with
 * the {@link PropertyValue#TARGET_PREFIX}) a dynamically evaluated value that represents the property in the context of
 * the provided entity's target.
 * <p>
 * For example:
 * foo = TARGET.bar
 * will set the current entity's 'foo' property to the value of it's target's 'bar' property.
 */
public class PropertyValue {
    private static final String TARGET_PREFIX = State.TARGET + ".";
    private static final Set<ValueType> PRIMITIVE_TYPES = Set.of(ValueType.FixedInteger, ValueType.FixedString, ValueType.Targeted);
    final ValueType type;
    private final Integer integerValue;
    private final String stringValue;
    private final Set<PropertyValue> setValue;
    private final String targetProperty;

    // initializes class fields for the given value type and value. assumes that value is of the correct type
    private PropertyValue(ValueType valueType, Object value) {
        this.type = valueType;
        switch (valueType) {
            case FixedInteger:
                integerValue = (Integer) value;
                stringValue = null;
                setValue = null;
                targetProperty = null;
                break;
            case FixedString:
                stringValue = (String) value;
                integerValue = null;
                setValue = null;
                targetProperty = null;
                break;
            case Targeted:
                targetProperty = (String) value;
                integerValue = null;
                setValue = null;
                stringValue = null;
                break;
            case Set:
                setValue = (Set<PropertyValue>) value;
                integerValue = null;
                stringValue = null;
                targetProperty = null;
                break;
            default:
                throw new IllegalStateException("PropertyValue ctor has unknown valueType " + valueType);
        }
    }

    public static PropertyValue string(String value) {
        return new PropertyValue(ValueType.FixedString, value);
    }

    public static PropertyValue integer(int value) {
        return new PropertyValue(ValueType.FixedInteger, value);
    }

    public static PropertyValue set(Set<PropertyValue> members) {
        return new PropertyValue(ValueType.Set, members);
    }

    public static PropertyValue parse(String value) {
        if (value.trim().startsWith(TARGET_PREFIX)) {
            final String targetProperty = value.substring(value.indexOf(".") + 1);
            if (targetProperty.isEmpty()) {
                throw new UnsupportedOperationException("target property is missing/empty");
            }

            return new PropertyValue(ValueType.Targeted, targetProperty);
        }

        // try to parse as integer, if it fails then it must be a string
        Object parsedValue = value;
        ValueType type = ValueType.FixedString;
        try {
            parsedValue = Integer.parseInt(value);
            type = ValueType.FixedInteger;
        } catch (NumberFormatException ignored) {
        }

        return new PropertyValue(type, parsedValue);
    }

    public static PropertyValue parseSet(String[] values) {
        if (values.length == 0) {
            throw new UnsupportedOperationException("contains operator needs at least 1 value");
        }

        Set<PropertyValue> setMembers = new HashSet<>();
        for (String token : values) {
            var prop = parse(token);
            if (!PRIMITIVE_TYPES.contains(prop.type)) {
                throw new UnsupportedOperationException(
                        "contains operator only allows integer/string/target values but got " + prop.type);
            }

            setMembers.add(prop);
        }

        return new PropertyValue(ValueType.Set, setMembers);
    }

    public Optional<Object> eval(State state, int eid) {
        switch (type) {
            case FixedInteger:
                assert integerValue != null;
                return Optional.of(integerValue);
            case FixedString:
                assert stringValue != null;
                return Optional.of(stringValue);
            case Targeted:
                Optional<Integer> targetEid = state.getTarget(eid);
                if (targetEid.isEmpty()) {
                    throw new IllegalStateException(
                            String.format("tried to fetch %s%s but entity has no target", TARGET_PREFIX, targetProperty));
                }

                return Optional.ofNullable(state.getProperty(targetEid.get(), targetProperty));
            case Set:
                assert setValue != null;
                return Optional.of(setValue);
            default:
                throw new IllegalStateException("PropertyValue eval has unknown valueType");
        }
    }

    public String toString() {
        switch (type) {
            case FixedInteger:
                assert integerValue != null;
                return integerValue.toString();
            case FixedString:
                return stringValue;
            case Targeted:
                return TARGET_PREFIX + targetProperty;
            case Set:
                assert setValue != null;
                return "{" + setValue.stream().map(PropertyValue::toString).collect(Collectors.joining(", ")) + "}";
            default:
                throw new IllegalStateException("PropertyValue toString has unknown evalType");
        }
    }

    public boolean isNumeric() {
        // we don't know if the target's property will be numeric, so we will have to catch this at runtime
        return type == ValueType.FixedInteger || type == ValueType.Targeted;
    }

    public boolean isTargeted() {
        switch (type) {
            case FixedInteger:
            case FixedString:
                return false;
            case Targeted:
                return true;
            case Set:
                assert setValue != null;
                return setValue.stream().anyMatch(PropertyValue::isTargeted);
        }
        return false;
    }

    enum ValueType {
        FixedInteger,
        FixedString,
        Targeted,
        Set,
    }
}
