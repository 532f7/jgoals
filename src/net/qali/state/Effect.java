package net.qali.state;


public class Effect {
    public final String property;
    public final Operator operator;
    public final PropertyValue value;

    private final boolean isNumeric;

    public Effect(String property, Operator operator, PropertyValue value) {
        this.property = property;
        this.operator = operator;

        this.isNumeric = operator == Operator.Add || operator == Operator.Subtract;

        if (isNumeric && !value.isNumeric()) {
            throw new UnsupportedOperationException(
                    String.format("invalid effect [%s %s %s]: cannot apply numeric operation '%s' to non-number '%s'",
                            property, operator, value, operator, value));
        }
        this.value = value;
    }

    public void apply(State state, int eid) {
        if (operator == Operator.Delete) {
            state.deleteProperty(eid, property);
            return;
        }

        var lhs = state.getProperty(eid, property);
        var maybeRhs = value.eval(state, eid);

        if (maybeRhs.isEmpty()) {
            throw new UnsupportedOperationException(String.format("unable to apply effect %s: right hand side %s is null",
                    this, value));
        }

        var rhs = maybeRhs.get();

        // if a numeric prop doesn't exist, set it to 0
        if (isNumeric && lhs == null) {
            lhs = 0;
        } else if (isNumeric && (lhs.getClass() != Integer.class || rhs.getClass() != Integer.class)) {
            String msg = String.format(
                    "unable to apply effect %s: numeric type mismatch for comparison %s %s %s",
                    this, lhs, operator, rhs);
            throw new UnsupportedOperationException(msg);
        }

        Object nextValue = null;
        switch (operator) {
            case Set:
                nextValue = this.value.eval(state, eid).orElse(null);
                break;
            case Add:
                nextValue = (Integer) lhs + (Integer) rhs;
                break;
            case Subtract:
                nextValue = (Integer) lhs - (Integer) rhs;
                break;
            default:
                throw new UnsupportedOperationException("Effect eval has unknown operator type");
        }

        state.setProperty(eid, property, nextValue);
    }

    public String toString() {
        return String.format("[%s %s %s]", property, operator, value);
    }

    /**
     * Returns whether or nor this effect's property value relies on properties from its entity's TARGET.
     *
     * @return
     */
    public boolean isTargeted() {
        return value.isTargeted();
    }

    // TODO: support delete / none operator?
    public enum Operator {
        Set,
        Add,
        Subtract,
        Delete,
    }
}