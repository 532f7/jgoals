package net.qali;

class ParsingException extends Exception {
    public ParsingException(int lineNum, String line, String error) {
        super(String.format("Error reading line %d '%s':\n\t%s", lineNum, line, error));
    }
}