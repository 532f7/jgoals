package net.qali;

import net.qali.scribe.Scribe;
import net.qali.state.Action;
import net.qali.state.State;

import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;
import java.util.stream.Collectors;

// handles FSM for a single entity
public class EntityExecutor {
    private static final Logger logger = Logger.getLogger(EntityExecutor.class.getName());

    public final int eid;
    private final Scribe scribe;
    private final List<Action> preActions;

    private ExecState execState = ExecState.Idle;
    private Planner.Plan plan;

    public EntityExecutor(int eid, Scribe scribe, State state) {
        this.eid = eid;
        this.scribe = scribe;
        this.preActions = state.getPreAction(eid).orElse(List.of());
    }

    private void log(State state, String msg) {
        var name = state.getName(eid);
        logger.info(String.format("[%s] %s", name, msg));
    }

    void tick(State state) {
        if (state.isProp(eid)) {
            return;
        }

        // double-check that this isn't actually a prop that doesn't have the property set
        List<Action> possibleActions = (List<Action>) state.getProperty(eid, State.ACTIONS);
        if (possibleActions.isEmpty()) {
            throw new IllegalArgumentException(String.format(
                    "error: entity '%s' has no actions defined. if this is meant to be a prop then set %s=true for this entity.",
                    state.getName(eid),
                    State.PROP));
        }

        switch (this.execState) {
            case Idle:
                Optional<Planner.Plan> maybePlan = Planner.plan(eid, state);
                if (maybePlan.isPresent()) {
                    plan = maybePlan.get();
                    assert plan.actions.size() > 0 : "empty plan for eid " + eid;
                    log(state, "selected plan " + plan.actions.stream().map(x -> x.name).collect(Collectors.joining(" > ")));
                    execState = ExecState.Executing;
                    scribe.logPlan(state, eid, plan);
                }
                break;
            case Executing:
                // apply pre-action actions
                for (Action pre : preActions) {
                    if (pre.possible(state, eid)) {
                        pre.apply(state, eid);
                    }
                }

                Action action = plan.actions.remove();
                // log action before performing the action. this is because the scribe logs events before they are
                // performed
                scribe.logAction(state, eid, action, plan);
                action.apply(state, eid);
                if (plan.actions.isEmpty()) {
                    execState = ExecState.Idle;
                }
                break;
        }
    }

    private enum ExecState {
        Idle,
        Executing,
    }
}
