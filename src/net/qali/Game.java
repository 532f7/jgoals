package net.qali;

import net.qali.scribe.Event;
import net.qali.scribe.Scribe;
import net.qali.state.State;

import java.io.IOException;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;

public class Game {
    private static final String ANSI_RESET = "\u001B[0m";
    private static final String ANSI_RED = "\u001B[31m";
    private static final String ANSI_GREEN = "\u001B[32m";
    private static final String ANSI_YELLOW = "\u001B[33m";
    private static final String ANSI_BLUE = "\u001B[34m";
    private static final String ANSI_PURPLE = "\u001B[35m";
    private static final String ANSI_CYAN = "\u001B[36m";
    private static final String ANSI_WHITE = "\u001B[1;37m";
    private static final String[] COLOURS = {ANSI_RED, ANSI_BLUE, ANSI_GREEN, ANSI_YELLOW, ANSI_PURPLE, ANSI_CYAN, ANSI_WHITE};
    private final State state;
    private final List<EntityExecutor> executors;
    private final Scribe scribe;

    public Game(State state) {
        this.state = state;
        scribe = new Scribe();
        executors = state.entities.keySet().stream()
                .map(eid -> new EntityExecutor(eid, scribe, state))
                .collect(Collectors.toList());
    }

    private String entityName(int eid) {
        return COLOURS[eid % COLOURS.length] + state.getName(eid) + ANSI_RESET;
    }

    private String cellName(int cid) {
        return ANSI_WHITE + state.getCellName(cid) + ANSI_RESET;
    }

    public void run() throws IOException {
        Scanner sc = new Scanner(System.in);
        int cid = state.randomCid();
        while (true) {
            for (var exec : executors) {
                exec.tick(state);
            }

            printCell(cid, scribe);
            cid = getNextCid(cid, sc);
        }
    }

    private int getNextCid(int currentCid, Scanner sc) {
        while (true) {
            System.out.print("\ngo to > ");
            try {
                String strNext = sc.nextLine().trim();
                if (strNext.isEmpty()) {
                    return currentCid;
                } else if (strNext.equals("q")) {
                    System.exit(0);
                }
                int next = Integer.parseInt(strNext);
                if (!state.getNeighbours(currentCid).contains(next)) {
                    System.out.printf("'%d' isn't connected to the current location\n", next);
                    continue;
                }

                return next;
            } catch (NumberFormatException e) {
                System.out.printf("can't read '%s' as a locationId\n", sc.next());
            }
        }
    }

    private void printCell(int cid, Scribe scribe) {
        System.out.println("You are at " + cellName(cid));
        Set<Integer> entities = state.entitiesAt(cid);
        if (!entities.isEmpty()) {
            System.out.print("over here there is:\n\t");
            for (int eid : entities) {
                System.out.print(entityName(eid) + " ");
            }
            System.out.println("\n");
        }

        // print events
        List<Event> events = scribe.getEventsAt(cid);
        for (Event e : events) {
            String entityName = entityName(e.eid);
            switch (e.type) {
                case Plan:
                    System.out.println(entityName + " thinks. ");
                    break;
                case Action:
                    if (e.target == null) {
                        System.out.printf("%s performs %s. ", entityName, e.actionName);
                    } else {
                        String targetName = entityName(e.target);
                        System.out.printf("%s performs %s on %s. ", entityName, e.actionName, targetName);
                    }
                    break;
                case Movement:
                    if (e.destinationCid == cid) {
                        // entity just entered
                        System.out.printf("%s enters from %s. ", entityName, cellName(e.originCid));
                    } else {
                        // exit
                        System.out.printf("%s exits to %s. ", entityName, cellName(e.destinationCid));
                    }
                    break;
            }
        }

        System.out.println();
        scribe.clearEvents();

        Set<Integer> neighbours = state.getNeighbours(cid);
        System.out.println("there are " + neighbours.size() + " connected areas:");
        for (int neighbour : neighbours) {
            System.out.println("\t" + neighbour + " " + cellName(neighbour));
        }
    }
}
