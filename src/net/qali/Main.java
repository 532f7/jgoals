package net.qali;

import net.qali.state.State;

import java.io.IOException;
import java.util.logging.Logger;

public class Main {
    private static Logger logger = Logger.getLogger(Main.class.getName());

    public static void main(String[] argv) {
        State state;

        ProgramArgs args = null;
        try {
            args = new ProgramArgs(argv);
        } catch (Exception e) {
            System.exit(1);
        }

        try {
            System.out.println("loading file " + args.filename);
            state = ConfigParser.loadStateFromFile(args.filename);
        } catch (IOException e) {
            System.err.println("unable to read file: " + e);
            return;
        } catch (ParsingException | IllegalArgumentException | UnsupportedOperationException e) {
            System.err.println("unable to parse file:\n\t" + e.getMessage());
            return;
        }

        Game game = new Game(state);
        try {
            game.run();
        } catch (IOException | RuntimeException e) {
            System.err.println("error");
            e.printStackTrace();
        }
    }
}
