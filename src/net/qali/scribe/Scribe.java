package net.qali.scribe;

import net.qali.Planner;
import net.qali.state.Action;
import net.qali.state.State;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Scribe {
    private final Map<Integer, List<Event>> events;

    public Scribe() {
        events = new HashMap<>();
    }

    private void addEvent(int cid, Event event) {
        events.putIfAbsent(cid, new ArrayList<>());
        events.get(cid).add(event);
    }

    public void logPlan(State state, int eid, Planner.Plan plan) {
        int cid = state.getCid(eid);
        addEvent(cid, Event.plan(state, eid, plan));
    }

    public void logAction(State state, int eid, Action action, Planner.Plan reason) {
        int cid = state.getCid(eid);
        Event evt = Event.action(state, eid, action, reason);

        // the above only logs cell exit, we need to log cell entry to the appropriate list
        // in case an entity chooses to enter our cell while we idle
        if (evt.type == Event.Type.Movement) {
            addEvent(evt.destinationCid, evt);
        }

        addEvent(cid, evt);
    }

    public List<Event> getEventsAt(int cid) {
        return events.getOrDefault(cid, new ArrayList<>());
    }

    public void clearEvents() {
        events.clear();
    }
}
