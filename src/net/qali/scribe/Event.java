package net.qali.scribe;

import net.qali.Planner;
import net.qali.state.Action;
import net.qali.state.State;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class Event {
    public final int eid;
    public final Type type;
    public final String actionName;
    public final String actionReason;
    public final List<String> plan;
    public final String planReason;
    public final Integer target;
    public final Integer originCid;
    public final Integer destinationCid;

    private Event(int eid, Type type, String actionName, String actionReason, Planner.Plan plan, Integer target, Integer originCid, Integer destinationCid) {
        this.eid = eid;
        this.type = type;
        this.actionName = actionName;
        this.actionReason = actionReason;
        this.target = target;
        this.originCid = originCid;
        this.destinationCid = destinationCid;

        if (plan != null) {
            this.plan = plan.actions.stream().map(x -> x.name).collect(Collectors.toList());
            this.planReason = plan.reason.name;
        } else {
            this.plan = null;
            this.planReason = null;
        }

    }

    public static Event action(State state, int eid, Action action, Planner.Plan reason) {
        Integer target = null;
        if (action.isTargeted()) {
            Optional<Integer> targetEid = state.getTarget(eid);
            assert targetEid.isPresent();

            target = targetEid.get();
        }

        Integer destinationCid = null;
        Integer originCid = state.getCid(eid);
        var movementEffect = action.effects.stream()
                .filter(e -> State.CID.equals(e.property))
                .findFirst();
        if (movementEffect.isPresent()) {
            // this must have a value because we just performed it
            destinationCid = (Integer) movementEffect.get().value.eval(state, eid).get();
        }

        Type type = destinationCid != null ? Type.Movement : Type.Action;
        return new Event(eid, type, action.name, reason.reason.name, null, target, originCid, destinationCid);
    }

    public static Event plan(State state, int eid, Planner.Plan plan) {
        return new Event(eid, Type.Plan, null, null, plan, null, null, null);
    }

    public String toString() {
        switch (type) {
            case Action:
                return String.format("\t%s because of plan %s", actionName, actionReason);
            case Plan:
                String plan = String.join(" > ", this.plan);
                return String.format("selected plan %s because of goal %s", plan, planReason);
            default:
                throw new IllegalStateException("Event toString unknown type " + type);
        }
    }

    public enum Type {
        Action,
        Plan,
        Movement,
    }
}
