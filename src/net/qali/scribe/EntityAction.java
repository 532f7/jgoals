package net.qali.scribe;

import net.qali.state.Action;
import net.qali.state.Prerequisite;

class EntityAction {
    public final String actionName;
    public final int eid;
    public final Prerequisite reason;

    public EntityAction(Action action, int eid, Prerequisite reason) {
        this.actionName = action.name;
        this.eid = eid;
        this.reason = reason;
    }
}